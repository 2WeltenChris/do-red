#!/usr/bin/env bash
# Update SIR
cd /workspace/do-red
npm install -g svelte-integration-red
npm install svelte-integration-red
# Install dependencies
npm install
# Link workspace to node-red
mkdir -p /home/gitpod/.node-red
cd /home/gitpod/.node-red/
npm install
npm link /workspace/do-red --save
# Go back to workspace directory
cd /workspace/do-red