# Changelog

## Version 1.0.7 (22th of August 2024)
- Replaced crypto.randomUUID() with a simple counter (https://gitlab.com/2WeltenChris/do-red/-/issues/10)
- Improved node UI description
- RED variable set as export to MarkInput (no more warnings on rendering)
- Task validation added

## Version 1.0.6 (12th of August 2024)
- bugfix missing onMount import

## Version 1.0.5 (06th of August 2024)
- added option to show/hide mark nodes

## Version 1.0.3 (05th of August 2024)
- mark do / do-return on selection fix

## Version 1.0.2 (29th of July 2024)
- mark do / do-return on selection fix (each mode)

## Version 1.0.1 (25th of July 2024)
- refactored rewire function

## Version 1.0.0 (24th of July 2024)
- latest SIR version
- added output rewire function
- mark do / do-return nodes on selection
- examples refactored
- added save to collection status

## Version 0.4.1 (26th of January 2024)
- added .npmignore (pictures)
- Bugfix: history entry for replaced node link 
- minified html files
- updated examples

## Version 0.4.0 (26th of January 2024)
- Corrected node version (new feature in 0.3.5)
- Bugfix: doubled nodes:add event emitted an append
- new / replaced nodes have an temporar _do_red key to identify nodes created via do-red in events

## Version 0.3.5 (25th of January 2024)
- Updated SIR and fixed UI bugs
- Bugfix: Node validation icon/each
- Added first / next 

## Version 0.3.4 (02nd of December 2021)
- Bugfix: no-op no longer deletes msg._do

## Version 0.3.3 (02nd of December 2021)

- changed optional chaining code to allow using node.js >v.14

## Version 0.3.2 (29th of November 2021)

- code cleaning
- renamed variables
- payload will now replace / delete the value in the collection
- if loop is an each-do, the do-return node can handle if the value should be replaced

## Version 0.3.1 (30th of August 2021)

- fused each and do node
- added do .doc file
- created return node with SIR

## Version 0.3 (27th of August 2021)

- Changed nested order (current do is now msg._do)
- Added each node

## Version 0.2.1 (26th of October 2020)

- Added rename-node-action
- Added replace-node-with action
- Added append-do-node action
- Added move-cursor actions

## Version 0.2.0 (28th of September 2020)

- Changed from node.send to send & done
- Added standard.js and formatted code
- Made no-op the default state for the do-node

## Version 0.1.7 (27th of August 2019)

- Example-flows can now be loaded through Import > Examples
- Added example for overlapping task-lists (e.g. to realize a kind of construction / building plan)