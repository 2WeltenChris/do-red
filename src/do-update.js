/* global RED */
// version === node version
const clientUpdate = (version, node) => {
  if (version.aggregated <= version.parse('0.3.5').aggregated) {
    // (old) changes before adding client update function
    if (typeof node.doneOutput === 'undefined') node.doneOutput = true
    if (typeof node.mode === 'undefined') node.mode = 'msg'
    if (typeof node.each === 'undefined') node.each = ''
    if (typeof node.eachType === 'undefined') node.eachType = 'msg'
    if (typeof node.yield === 'undefined') node.yield = 'keyValue'
    // revalidate after timeout to hide error
    setTimeout(() => {
      RED.editor.validateNode(RED.nodes.node(node.id))
      RED.view.select()
    }, 2000)
    // real v. 0.3.5 changes
    if (typeof node.firstValueOutput === 'undefined') node.firstValueOutput = false
    if (typeof node.lastValueOutput === 'undefined') node.lastValueOutput = false
  }
  return node
}

module.exports = { clientUpdate }
